package com.eudemon.taurus.app.service;

import java.io.ByteArrayInputStream;
import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.PutObjectRequest;

/**
 * OSS service using the Aliyun OSS SDK.
 */
@Service
public class OSSService {
	private Logger logger = LoggerFactory.getLogger(OSSService.class);

	private static String endpoint = "http://bucket-wjb.oss-cn-beijing-xhcloud-d01-a.xhcloud.news.cn";
	private static String accessKeyId = "mRwYQoctaXDPVoVQ";
	private static String accessKeySecret = "EjNDsX4FVUOk8qgqWJLKfo5azT1bt6";
	
	/**
	 * upload an object to Aliyun OSS
	 * @param file
	 * @param bucketName
	 * @param key
	 * @throws Exception
	 */
	public void uploadObject(MultipartFile file, String bucketName, String key) throws Exception {
		OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);

		try {
			client.putObject(new PutObjectRequest(bucketName, key, new ByteArrayInputStream(file.getBytes())));
		} catch (OSSException oe) {
			logger.error("OSSService uploadObject OSSException : " + oe.getMessage(), oe);
			throw new Exception("OSSService uploadObject OSSException : " + oe.getMessage(), oe);
		} catch (ClientException ce) {
			logger.error("OSSService uploadObject ClientException : " + ce.getMessage(), ce);
			throw new Exception("OSSService uploadObject ClientException : " + ce.getMessage(), ce);
		} catch (Exception e) {
			logger.error("OSSService uploadObject Exception : " + e.getMessage(), e);
			throw new Exception("OSSService uploadObject Exception : " + e.getMessage(), e);
		} finally {
			client.shutdown();
		}
	}

	/**
	 * download an object from Aliyun OSS
	 * 
	 * @param ojbName
	 */
	public void downloadObject(String bucketName, String ojbName) throws Exception {
		OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);

		try {
			client.getObject(new GetObjectRequest(bucketName, ojbName), new File(ojbName));
		} catch (OSSException oe) {
			logger.error("OSSService downloadObject OSSException : " + oe.getMessage(), oe);
			throw new Exception("OSSService downloadObject OSSException : " + oe.getMessage(), oe);
		} catch (ClientException ce) {
			logger.error("OSSService downloadObject ClientException : " + ce.getMessage(), ce);
			throw new Exception("OSSService downloadObject ClientException : " + ce.getMessage(), ce);
		} catch (Exception e) {
			logger.error("OSSService downloadObject Exception : " + e.getMessage(), e);
			throw new Exception("OSSService downloadObject Exception : " + e.getMessage(), e);
		} finally {
			client.shutdown();
		}
	}
	
	public void uploadObjectWjb(MultipartFile file, String ossPath) throws Exception {
		OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		
		try {
			String bucketName = "bucket-wjb";
            String originalFilename = file.getOriginalFilename();
            String fileType = originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();
            String key = ossPath + System.currentTimeMillis() + "_" + originalFilename;
            
			client.putObject(new PutObjectRequest(bucketName, key, new ByteArrayInputStream(file.getBytes())));
		} catch (OSSException oe) {
			logger.error("OSSService uploadObject OSSException : " + oe.getMessage(), oe);
			throw new Exception("OSSService uploadObject OSSException : " + oe.getMessage(), oe);
		} catch (ClientException ce) {
			logger.error("OSSService uploadObject ClientException : " + ce.getMessage(), ce);
			throw new Exception("OSSService uploadObject ClientException : " + ce.getMessage(), ce);
		} catch (Exception e) {
			logger.error("OSSService uploadObject Exception : " + e.getMessage(), e);
			throw new Exception("OSSService uploadObject Exception : " + e.getMessage(), e);
		} finally {
			client.shutdown();
		}
	}
	
	public void downloadObjectWjb(String ojbName) throws Exception {
		OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);

		try {
			String bucketName = "bucket-wjb";
			logger.debug("[downloadObjectWjb start] objName = " + ojbName);
			client.getObject(new GetObjectRequest(bucketName, ojbName), new File(ojbName));
		} catch (OSSException oe) {
			logger.error("OSSService downloadObject OSSException : " + oe.getMessage(), oe);
			throw new Exception("OSSService downloadObject OSSException : " + oe.getMessage(), oe);
		} catch (ClientException ce) {
			logger.error("OSSService downloadObject ClientException : " + ce.getMessage(), ce);
			throw new Exception("OSSService downloadObject ClientException : " + ce.getMessage(), ce);
		} catch (Exception e) {
			logger.error("OSSService downloadObject Exception : " + e.getMessage(), e);
			throw new Exception("OSSService downloadObject Exception : " + e.getMessage(), e);
		} finally {
			client.shutdown();
			logger.debug("[downloadObjectWjb finish] objName = " + ojbName);
		}
	}
}