package com.eudemon.taurus.app.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Hashtable;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;

import org.springframework.stereotype.Service;

@Service
public class WeblogcMonitorService {
	private static final ObjectName DomainRuntimeServiceMBean;
	private static final ObjectName RuntimeServiceMBean;
	static {
		try {
			// 服务名称
			DomainRuntimeServiceMBean = new ObjectName(
					"com.bea:Name=DomainRuntimeService,Type=weblogic.management.mbeanservers.domainruntime.DomainRuntimeServiceMBean");
		} catch (MalformedObjectNameException e) {
			throw new AssertionError(e.getMessage());
		}
	}
	static {
		try {
			RuntimeServiceMBean = new ObjectName(
					"com.bea:Name=RuntimeService,Type=weblogic.management.mbeanservers.runtime.RuntimeServiceMBean");
		} catch (MalformedObjectNameException e) {
			throw new AssertionError(e.getMessage());
		}
	}

	public JMXConnector getRuntimeServiceConnector(String hostname, int port, String username, String password)
			throws IOException, MalformedURLException {
		String protocol = "t3";
		String jndiroot = "/jndi/";
		String mserver = "weblogic.management.mbeanservers.runtime";
		JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostname, port, jndiroot + mserver);
		Hashtable<String, String> h = new Hashtable<String, String>();
		h.put(Context.SECURITY_PRINCIPAL, username);
		h.put(Context.SECURITY_CREDENTIALS, password);
		h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");
		JMXConnector connector = JMXConnectorFactory.connect(serviceURL, h);
		return connector;
	}
	
	public JMXConnector getDomainRuntimeServiceConnector(String hostname, int port, String username, String password)
			throws IOException, MalformedURLException {
		String protocol = "t3";
		String jndiroot = "/jndi/";
		String mserver = "weblogic.management.mbeanservers.domainruntime";
		JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostname, port, jndiroot + mserver);
		Hashtable<String, String> h = new Hashtable<String, String>();
		h.put(Context.SECURITY_PRINCIPAL, username);
		h.put(Context.SECURITY_CREDENTIALS, password);
		h.put(JMXConnectorFactory.PROTOCOL_PROVIDER_PACKAGES, "weblogic.management.remote");
		JMXConnector connector = JMXConnectorFactory.connect(serviceURL, h);
		return connector;
	}

	public Object getAttribute(JMXConnector connector, String tabName, String tableColumn)
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException,
			IOException {
		MBeanServerConnection connection = connector.getMBeanServerConnection();
		ObjectName serverRuntime = (ObjectName) connection.getAttribute(RuntimeServiceMBean, "ServerRuntime");
		Object value = connection.getAttribute((ObjectName) connection.getAttribute(serverRuntime, tabName),
				tableColumn);

		return value;
	}

	public void test(JMXConnector connector) throws AttributeNotFoundException, InstanceNotFoundException,
			MBeanException, ReflectionException, IOException {
		MBeanServerConnection connection = connector.getMBeanServerConnection();
		ObjectName[] serverRuntimes = (ObjectName[]) connection.getAttribute(DomainRuntimeServiceMBean, "ServerRuntimes");
		for (ObjectName serverRuntime : serverRuntimes) {
			Object value = connection.getAttribute(
					(ObjectName) connection.getAttribute(serverRuntime, "ThreadPoolRuntime"), "HealthState");

			System.out.println(value);
		}
	}

	public String getAttributeThreadPoolRuntime(JMXConnector connector, String tableColumn)
			throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException,
			IOException {
		String value = String.valueOf(getAttribute(connector, "ThreadPoolRuntime", tableColumn));

		return value;
	}

	public static void main(String[] args) {
		WeblogcMonitorService sv = new WeblogcMonitorService();
		JMXConnector connector = null;
		JMXConnector domainConnector = null;
		try {
			connector = sv.getRuntimeServiceConnector("192.168.86.52", 7001, "weblogic", "wls!@#$%");
			String throughput = sv.getAttributeThreadPoolRuntime(connector, "Throughput");
			System.out.println("Throughput:" + throughput);
			String ExecuteThreadTotalCount = sv.getAttributeThreadPoolRuntime(connector, "ExecuteThreadTotalCount");
			System.out.println("ExecuteThreadTotalCount:" + ExecuteThreadTotalCount);
			String PendingUserRequestCount = sv.getAttributeThreadPoolRuntime(connector, "PendingUserRequestCount");
			System.out.println("PendingUserRequestCount:" + PendingUserRequestCount);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AttributeNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstanceNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MBeanException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ReflectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (null != connector) {
					connector.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
